**Blackstone's Legacy 2020 Berlin campaign report**

## Campaign days

| Date               | Time        | Played Ingame Days (IgDay) |
| ---                | ---         | ---                        |
| Sa, 11th Jan 2020  | 11am to 7pm | 01                         |
| Sa, 25th Jan 2020  | 11am to 7pm | 02                         |
| Sa, 1st Feb 2020   | 11am to 7pm | ?                          |
| Sa, 7th Mar 2020   | 11am to 7pm | ?                          |
| Sa, 4th Apr 2020   | 11am to 7pm | ?                          |
| Sa, 2nd May 2020   | 11am to 7pm | ?                          |
| Sa, 27th June 2020 | 11am to 7pm | ?                          |

- Org. date vote: https://poll.digitalcourage.de/ZrVZU1CZmDPgdJ7x

# Participants (in alphabetical order)

| Player name | Faction                                  | Kill Team name         | Kill Team ID | Participation on IgDays |
| ---         | ---                                      | ---                    | ---          | ---                     |
| Ali         | Necrons                                  | FluffyGismos           | FG           | 01, 02                  |
| Duc         | Drukhari                                 | No Pain No Gain        | NPNG         | 01                      |
| Filip       | Death Watch                              | Garbage Collection     | GC           | 02                      |
| Julian      | Orks                                     | Lucky Looters          | LL           | 01, 02                  |
| Kilian      | Death Guard                              | Lepra-Express          | LPX          | 02                      |
| Nuri        | Catachan Ork Hunters (Genestealer Cults) | ?                      | ?            | 02                      |
| Philipp     | Adeptus Astartes                         | Soriel's Battlegroup   | SB           | 01, 02                  |
| Roland      | T'au Empire                              | For the Greater Legacy | FtGL         | -                       |

# Ranking

## Team ranking

### Blackstone Fortress missions

| Kill Team ID | Mission Glory |
| ---          | ---           |
| FG           | 2             |
| GC           | 1             |
| LL           | -1            |
| LPX          | -1            |
| NPNG         | -1            |
| SB           | -2            |

### Precipice Arena matches

| Kill Team ID | Arena Team Kills | Total Arena Team Glory |
| ---          | ---              | ---                    |
| FG           | 2                | 4                      |
| LPX          | 0                | 1                      |
| SB           | 1                | 0                      |
| GC           | 0                | 0                      |
| LL           | 0                | -1                     |
| NPNG         | 0                | -2                     |

## Single ranking (Arena plot only)

| Character name   | CMC ID | Sub-Faction  | Arena Unit Kills | Arena Unit Glory |
| ---              | ---    | ---          | ---              | ---              |
| Eknotath         | FG2    | Novokh       | 2                | +4               |
| Ezekial Kaelon   | SB03   | Dark Angels  | 1                | 1                |
| Fridbert         | LPX06  | Dark Raiders | 0                | 1                |
| ?                | GC07   | -            | 0                | 0                |
| Boltz            | LL07   | Deathskulls  | 0                | -1               |
| Nemator Azdallon | SB02   | Dark Angels  | 0                | -1               |
| Edward           | NPNG01 | ???          | 0                | -2               |

# Mission reports

## Ingame Day 01 (2020-01-11)

### Blackstone Fortress missions

#### "Soriel's Battlegroup" (SB) VS "Garbage Collection" (GC)

[![bslegacy2020-01-11-berlin_igday01_12-47-23_cut400.jpg](media/bslegacy2020-01-11-berlin_igday01_12-47-23_cut400.jpg "BSLegacy ingame day 01 - Soriel's Battlegroup VS Filip's Space Marines")](media/bslegacy2020-01-11-berlin_igday01_12-47-23_cut.jpg)

- Mission: Territorial Imperative
- Result: GC won
- Anomalies: The Dreaded Ambul appeared.

#### "FluffyGismos" (FG) VS "NoPainNoGain" (NPNG) VS "Lucky Looters" (LL)

[![bslegacy2020-01-11-berlin_igday01_11-46-42_cut400](media/bslegacy2020-01-11-berlin_igday01_11-46-42_cut400.jpg "BSLegacy ingame day 01 - FluffyGismos VS No Pain No Gain VS an Ork Kill Team")](media/bslegacy2020-01-11-berlin_igday01_11-46-42_cut.jpg)

- Mission: Hidden Vault Datacube Hunt
- Result: FG won

### Precipice Arena matches

- Adeptus Astartes "Ezekial Kaelon" (SB03) won against Ork "Boltz" (LL01)
- Necron "Eknotath" (FG2) won against Drukhari "Edward" (NPNG01) and Adeptus Astartes ??? (GC07)

## Ingame Day 02 (2020-01-25)

### Blackstone Fortress missions

#### "Soriel's Battlegroup" (SB) VS (Nuri)

- Mission: ???
- Result: ???
- Anomalies: The Dreaded Ambul appeared.

#### "FluffyGismos" (FG) VS "Lucky Looters" (LL)

- Mission:
- Result: FG won
- Anomalies: Cultist Firebrand appeared and joined LL, Lost Negavolt Cultist appeared and attcked FG.

#### "Garbage Collection" (GC) "Lepra-Express" (LPX)

- Mission: Territorial Imperative
- Result: GC won
- Anomalies: A Bloodchaser appeared, but was immediately gunned down by one of the death watch artillery.
- Optional: The special Death Watch unit called "Garbage Collection" was destined to meet their end against the "Lepra-Express" (The unstoppable force of death and disease, under direct supervision of Nurgle.) But Fate decided to side with the Death Watch. Right after the Death Guards front lines of cannon fodder, led by the rotten Constantin the I., entered the dungeon, they were stopped in the midst of their advances. The Garbage Collection knew, that once their miasma started spreading, there would be no chance of retribution. So they've acted swiftly and precise, like a precisely calculated and quick stab with a shiv. Their specialist for melee combat, armed with a heavy shield, withheld the advancements of the Poxwalkers - one route was successfully blocked. Immediately Nurgles troops stormed towards the other corridor, only to be awaited by the Death Watches Exploration unit - a skilled warrior equipped with one of the fastes jetpacks to exist. Noone knew how he had managed to block the second corridor, before even any of Lepra-Express had a chance to notice. Packed like sardines, Nurgles troops had nowhere to go. They couldn't advance and the second wave of troops could not enter the dungeon. This was exactly what the Garbage Collection wanted. The Poxwalkers could not fullfill their mission and storm the area. But after a quick moment of irritation, they were ready to club their way through those pesky Marines. Unfortunately for the Lepra Express, the heavy artillery of the Grabage Collection has already moved into position to unleash a volley of bullets noone could survive. The undead Constantin the I. had immediately noticed what was going on and already readied his plasmagun to take out the Death Guards back line to prevent them from fireing. Because even with the Dath Guards disgustingly resilient bodily constitution, the inevitable bullet hell, which was about to be unleashed, would rip even the toughest of his troops to shreds. So Constanting acted just as swiftly. Even before the first Marine got a chance to fire, Constantin the I. managed to critically injure the Death Watches Combat specialist, who was wielding one of the three frag cannon, which were destined to annihilate everything. But even in death the Garbage Collection would fullfill their duty. The falling marine, full of ire, fired into the enemy troups, but didn't manage to kill Constantin. But immediately, after the second two Gunners realized what just happened, they as well started fireing. And the sounds of the ripping flesh from the rotten bodies of the Lepra-Express was re-echoing all over the dungeon. Noone could've survived that. They've managed to take Constantin out of action. But alas, the poxwalkers - only slightly decimated - started their onslaught. They've managed to kill the Garbage Collections special exploration unit, but had to fall in the process. After that hectic battle the Lepra Express was almost completely anihilated, only three broken Poxwalker were left. They ran for their lives and saved everyone they could. It was a clear victory for the Garbage Collection, although they've tragically lost one of their specialists.

### Precipice Arena matches

- Necron "Eknotath" (FG02) lost against ???
- Adeptus Astartes "Nemator Azdallon" (SB02) lost against ???
